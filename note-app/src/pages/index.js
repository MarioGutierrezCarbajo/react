import Layout from "../components/Layout/Layout.jsx";
import styles from "../styles/Home.module.css";
import SearchInput from "../components/SearchInput/SearchInput.jsx";
import NotesTable from "../components/NotesTable/NotesTable.jsx";
import AddNote from "../components/AddNote/AddNote.jsx";
import About from "../components/About/About.jsx";
import { useState } from "react";

export default function Home({ notes }) {
    const [keyword, setKeyword] = useState("");

    const filteredNotes = notes.filter(
        (note) =>
            note.description.toLowerCase().includes(keyword) ||
            note.to.toLowerCase().includes(keyword) ||
            note.from.toLowerCase().includes(keyword) ||
            note.done.toString().toLowerCase().includes(keyword)
    );

    const onInputChange = (e) => {
        e.preventDefault();
        setKeyword(e.target.value.toLowerCase());
    };

    return (
        <Layout>
            <div className={styles.counts}>Found {notes.length} notes</div>
            <SearchInput
                placeholder="Filter by description"
                onChange={onInputChange}
            />
            <NotesTable notes={filteredNotes} />
        </Layout>
    );
}

export const getStaticProps = async () => {
    const res = await fetch("http://localhost:3333/api/notes/");
    const notes = await res.json();
    return {
        props: {
            notes,
        },
    };
};
