import Layout from "../../components/Layout/Layout";
import styles from "./note.module.css";

const Note = ({ note }) => {
    console.log(note);
    var description = note[0].description;
    return (
        <Layout title={note[0].description}>
            <div>
                <div className={styles.overview_panel}>
                    <h1 className={styles.overview_name}>
                        {note[0].description}
                    </h1>
                    <div className={styles.overview_region}>
                        Done: {note[0].done.toString()}
                    </div>
                    <div className={styles.overview_numbers}>
                        <div className={styles.overview_population}>
                            <div className={styles.overview_value}>
                                From: {note[0].from}
                            </div>
                        </div>
                        <div className={styles.overview_area}>
                            <div className={styles.overview_value}>
                                To: {note[0].to}
                            </div>
                        </div>
                    </div>
                    <div className={styles.overview_region}>
                        <button
                            disabled={note[0].done}
                            onClick={()=>doneNote(description)}
                        >
                            Done
                        </button>
                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default Note;

const doneNote = async (description) => {
    const requestOptions = {
        method: 'PUT',
    };
    console.log("description: " + description);
    var done = await fetch("http://localhost:3333/api/notes/done/name/"+ description,requestOptions);
    console.log("done: " + done);
    window.location.reload(false);
}

export const getServerSideProps = async ({ params }) => {
    const res = await fetch(
        `http://localhost:3333/api/notes/name/${params.id}`
    );

    const note = await res.json();
    return {
        props: {
            note,
        },
    };
};
