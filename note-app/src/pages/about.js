import Layout from "../components/Layout/Layout.jsx";
import About from "../components/About/About.jsx";

export default function Home() {

    return (
        <Layout>
            <About></About>
        </Layout>
    );
}