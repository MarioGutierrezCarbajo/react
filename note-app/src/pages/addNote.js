import Layout from "../components/Layout/Layout.jsx";
import AddNote from "../components/AddNote/AddNote.jsx";

export default function Home() {

    return (
        <Layout>
            <AddNote></AddNote>
        </Layout>
    );
}