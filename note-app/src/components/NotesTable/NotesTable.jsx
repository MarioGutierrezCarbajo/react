//NotesTable.jsx
import styles from "./NotesTable.module.css";
import Link from "next/link";

const NotesTable = ({ notes }) => {
    return (
        <div>
            <div className={styles.heading}>
                <button className={styles.header_description}>
                    <div>Description</div>
                </button>
                <button className={styles.header}>
                    <div>From</div>
                </button>
                <button className={styles.header}>
                    <div>To</div>
                </button>
            </div>

            {notes.map((notes) => (
                <Link
                    key={notes.description}
                    href={`/note/${notes.description}`}
                >
                    <div
                        className={
                            styles.row +
                            " " +
                            (notes.done === true
                                ? styles.done
                                : styles.undone)
                        }
                        key={notes.description}
                    >
                        <div className={styles.description}>
                            {notes.description}
                        </div>
                        <div className={styles.secondary}>{notes.from}</div>
                        <div className={styles.secondary}>{notes.to}</div>
                    </div>
                </Link>
            ))}
        </div>
    );
};

export default NotesTable;
