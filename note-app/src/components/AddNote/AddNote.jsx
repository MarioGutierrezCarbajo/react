import React from "react";
import style from "./AddNote.module.css";
import { useFormik } from "formik";

function AddNote() {
    const formik = useFormik({
        initialValues: {
            description: "",
            from: "",
            to: "",
        },
        onSubmit: (values) => {
            const requestOptions = {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(values),
            };
            fetch("http://localhost:3333/api/notes/", requestOptions)
                .then((response) => response.json())
                .then((data) => {
                    console.log(data);
                    window.location.reload(false);
                });
        },
    });
    return (
        <div className="addNote">
            <h1 className={style.h1}>Add Note</h1>
            <form className={style.form} onSubmit={formik.handleSubmit}>
                <label className={style.label} htmlFor="description">
                    Description
                </label>
                <input
                    className={style.label}
                    id="description"
                    name="description"
                    onChange={formik.handleChange}
                    value={formik.values.description}
                />
                <label className={style.label} htmlFor="from">
                    From
                </label>
                <input
                    className={style.label}
                    id="from"
                    name="from"
                    onChange={formik.handleChange}
                    value={formik.values.from}
                />
                <label className={style.label} htmlFor="to">
                    To
                </label>
                <input
                    className={style.label}
                    id="to"
                    name="to"
                    onChange={formik.handleChange}
                    value={formik.values.to}
                />
                <button className={style.button} type="submit">
                    Save
                </button>
            </form>
        </div>
    );
}

export default AddNote;
